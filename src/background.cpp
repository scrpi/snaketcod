#include "background.hpp"

Background::Background()
{
    random = TCODRandom::getInstance();
    noise = new TCODNoise(3);
    offset = 0.0f;
}

void Background::Init()
{
    int x = TCODConsole::root->getWidth() * 2;
    int y = TCODConsole::root->getHeight() * 2;
    img = new TCODImage(x, y);
}

void Background::Update()
{
    int i, j;
    for (i = 0; i < TCODConsole::root->getWidth() * 2; i++) {
        for (j = 0; j < TCODConsole::root->getHeight() * 2; j++) {
            float f[3], val;
            f[0] = i / 20.0f;
            f[1] = j / 20.0f;
            f[2] = offset;
            val = noise->getFbm(f, 4);
            val = (val + 1.0f) / 2.0f;
            img->putPixel(i,j,TCODColor(0, (int)(32.0f * val), (int)(64.0f * val)));
        }
        offset += TCODSystem::getLastFrameLength() / 128;
    }
}

void Background::Render()
{
    img->blit2x(TCODConsole::root,0,0);
}
