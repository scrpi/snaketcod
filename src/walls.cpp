#include <iostream>
#include "walls.hpp"
#include "game.hpp"

WallCell::WallCell(int loc_x, int loc_y)
{
    x = loc_x;
    y = loc_y;
}

void Walls::Init()
{
    int i;
    for (i = 0; i < MAP_W; i++) {
        cells.push_back(WallCell(i, 0));
        cells.push_back(WallCell(i, MAP_H - 1));
    }
    for (i = 0; i < MAP_H; i++) {
        cells.push_back(WallCell(0, i));
        cells.push_back(WallCell(MAP_W - 1, i));
    }
}

void Walls::Render()
{
    for (int i = 0; i < (int)cells.size(); i++) {
        WallCell cell = cells.at(i);
        TCODConsole::root->setCharBackground(cell.x, cell.y, TCODColor::darkAzure);
        TCODConsole::root->putChar(cell.x, cell.y, ' ');
    }
}

bool Walls::IsWall(int sx, int sy)
{
    for (int i = 0; i < (int)cells.size(); i++) {
        if (sx == cells.at(i).x && sy == cells.at(i).y)
            return true;
    }
    return false;
}
