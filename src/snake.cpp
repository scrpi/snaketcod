#include <iostream>
#include <vector>
#include "libtcod.hpp"
#include "game.hpp"
#include "snake.hpp"

SnakeSegment::SnakeSegment(int loc_x, int loc_y)
{
    x = loc_x;
    y = loc_y;
}

Snake::Snake()
{

}

void Snake::spawn(int loc_x, int loc_y, int length, int direction, float cellsPerSecond)
{
    x             = loc_x;
    y             = loc_y;
    dir           = direction;
    newDir1       = dir;
    newDir2       = dir;
    speed         = 1 / cellsPerSecond;
    timeSinceMove = 0.0f;
    body.clear();

    // Spawn the snake with 0 food.
    food = 0;

    for (int i = 0; i < length; i++) {
        int seg_x = x;
        int seg_y = y;
        switch(dir) {
            case DIR_U: seg_y += i; break;
            case DIR_D: seg_y -= i; break;
            case DIR_L: seg_x += i; break;
            case DIR_R: seg_x -= i; break;
            default: break;
        }
        body.push_back(SnakeSegment(seg_x, seg_y));
    }
}

void Snake::loop(SnakeGame * game)
{
    timeSinceMove += TCODSystem::getLastFrameLength();
    if (timeSinceMove > speed) {
        timeSinceMove -= speed;

        /*
         * Calculate the new snake position
         * We use two 'newDir' variables to keep track of two key presses
         * at a time.
         * This fixes a bug where by if the player presses two directions
         * within the same snake 'tick' it will only register the last keypress.
         * This can potentiall kill the snake if the last keypress is turning
         * into itself.
         */
        int oldDir = dir;
        dir        = newDir1;
        newDir1    = newDir2;

        // Don't let the player do a 180.
        switch(oldDir) {
            case DIR_U: if (dir == DIR_D) dir = oldDir; break;
            case DIR_D: if (dir == DIR_U) dir = oldDir; break;
            case DIR_L: if (dir == DIR_R) dir = oldDir; break;
            case DIR_R: if (dir == DIR_L) dir = oldDir; break;
            default: break;
        }

        int seg_x = x;
        int seg_y = y;
        switch(dir) {
            case DIR_U: seg_y--; break;
            case DIR_D: seg_y++; break;
            case DIR_L: seg_x--; break;
            case DIR_R: seg_x++; break;
            default: break;
        }

        // Check if the new location is valid
        if (!isBody(seg_x, seg_y) && !game->walls.IsWall(seg_x, seg_y)) {
            // Insert a new segment and update head position
            body.insert(body.begin(), SnakeSegment(seg_x, seg_y));
            x = seg_x;
            y = seg_y;

            // Food?
            if (game->food->x == seg_x && game->food->y == seg_y) {
                eat(game->food->size);
                game->eatFood();
            }

            if (food > 0) {
                // Eat one food. Do not remove from tail (snake grows longer)
                food--;
            } else {
                // No food to eat, need to remove from tail.
                body.pop_back();
            }
        } else {
            // We must have hit a wall or ourself. Game Over
            game->die();
        }
    }
}

void Snake::render()
{
    for (int i = 0; i < (int)body.size(); i++) {
        SnakeSegment seg = body.at(i);
        TCODConsole::root->setCharBackground(seg.x, seg.y, TCODColor::yellow);
        TCODConsole::root->putChar(seg.x, seg.y, ' ');
    }
}

void Snake::changeDir(int direction)
{
    if (newDir1 == dir) {
        newDir1 = direction;
        newDir2 = direction;
    } else {
        newDir2 = direction;
    }
}

void Snake::eat(int num)
{
    food += num;
}

bool Snake::isBody(int x, int y)
{
    for (int i = 0; i < (int)body.size(); i++) {
        if (x == body.at(i).x && y == body.at(i).y)
            return true;
    }
    return false;
}
