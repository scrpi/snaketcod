#include <iostream>
#include <cstdlib>

#include "game.hpp"

SnakeGame::SnakeGame()
{
    run = true;
}

bool SnakeGame::init()
{
    TCODConsole::initRoot(MAP_W, MAP_H , "Snake", false);
    paused = false;
    dead = true;
    bg = Background();
    bg.Init();
    walls = Walls();
    walls.Init();
    snake = Snake();
    startNewGame();
    return true;
}

void SnakeGame::event()
{
    if (TCODConsole::isWindowClosed()) {
        run = false;
        return;
    }

    TCOD_key_t key;
    TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS, &key ,NULL);
    switch (key.vk) {
        case TCODK_UP    : snake.changeDir(DIR_U); break;
        case TCODK_DOWN  : snake.changeDir(DIR_D); break;
        case TCODK_LEFT  : snake.changeDir(DIR_L); break;
        case TCODK_RIGHT : snake.changeDir(DIR_R); break;
        case TCODK_SPACE : if (dead == true) restartGame(); break;
        case TCODK_ESCAPE: run = false; break;
        default: break;
    }
    switch (key.c) {
        case 'p': paused = !paused; break;
        default: break;
    }
}

void SnakeGame::loop()
{
    if (paused == false && dead == false)
        snake.loop(this);
        bg.Update();
}

void SnakeGame::render()
{
    TCODConsole::root->clear();

    bg.Render();
    walls.Render();
    snake.render();

    TCODConsole::root->putCharEx(food->x, food->y, '*', TCODColor::magenta, TCODColor::black);
    if (paused == true) {
        TCODConsole::root->setDefaultForeground(TCODColor::white);
        TCODConsole::root->print(MAP_W - 8, 2, "PAUSED");
    }

    if (dead == true) {
        TCODConsole::root->setAlignment(TCOD_CENTER);
        TCODConsole::root->setDefaultForeground(TCODColor::red);
        TCODConsole::root->print(MAP_W / 2, MAP_H / 2 + 2, "GAME OVER");
        TCODConsole::root->setDefaultForeground(TCODColor::white);
        TCODConsole::root->print(MAP_W / 2, MAP_H / 2 + 4, "Press space-bar to start again!");
    }
}

int SnakeGame::runApp()
{
    if (init() == false) {
        return -1;
    }

    while (run) {
        event();
        loop();
        render();
        TCODConsole::flush();
    }
    return 0;
}

void SnakeGame::spawnFood(int size)
{
    bool spawned = false;
    int x;
    int y;

    while (!spawned) {
        x = rand() % (MAP_W - 1);
        y = rand() % (MAP_H - 1);
        if (!snake.isBody(x, y) && !walls.IsWall(x, y))
            spawned = true;
    }

    food = new Food(x, y, size);
}

void SnakeGame::die()
{
    dead = true;

}

void SnakeGame::restartGame()
{
    delete food;
    startNewGame();
}

void SnakeGame::startNewGame()
{
    snake.spawn(MAP_W / 2, MAP_H / 2, 10, DIR_U, 15);
    spawnFood(15);
    dead = false;
}

void SnakeGame::eatFood()
{
    delete food;
    spawnFood(10);
}

int main() {
    SnakeGame sg;

    sg.runApp();
}
