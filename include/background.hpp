#ifndef _BACKGROUND_H_
#define _BACKGROUND_H_

#include "libtcod.hpp"

class Background
{
private:
    TCODImage * img;
    TCODNoise * noise;
    TCODRandom * random;
    float offset;
public:
    Background();
    void Init();
    void Update();
    void Render();
};

#endif // _BACKGROUND_H_
