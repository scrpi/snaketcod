#ifndef _SNAKE_H_
#define _SNAKE_H_

#include <vector>
#include "game.hpp"

#define DIR_U 0
#define DIR_R 1
#define DIR_D 2
#define DIR_L 3

class SnakeGame;

class SnakeSegment
{
public:
    SnakeSegment(int loc_x, int loc_y);
    int x;
    int y;
};

class Snake
{
private:
    std::vector<SnakeSegment> body;
    int x;
    int y;
    int dir;
    int newDir1;
    int newDir2;
    int food;
    float speed;
    float timeSinceMove;
public:
    Snake();
    void spawn(int loc_x, int loc_y, int length, int direction, float cellsPerSecond);
    void loop(SnakeGame * game);
    void render();
    void changeDir(int direction);
    void eat(int num);
    bool isBody(int x, int y);
};

#endif // _SNAKE_H_
