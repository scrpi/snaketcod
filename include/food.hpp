#ifndef _FOOD_H_
#define _FOOD_H_

class Food
{
public:
    Food(int loc_x, int loc_y, int num);
    int size;
    int x;
    int y;
};

#endif // _FOOD_H_
