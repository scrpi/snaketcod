#ifndef _GAME_H_
#define _GAME_H_

#include <vector>
#include "libtcod.hpp"
#include "snake.hpp"
#include "food.hpp"
#include "background.hpp"
#include "walls.hpp"

#define MAP_W 80
#define MAP_H 50

class SnakeGame
{
private:
    bool run;
    int px;
    int py;
    Snake snake;
    bool paused;
    bool dead;
    void spawnFood(int size);
    Background bg;
    void startNewGame();
public:
    SnakeGame();
    Food * food;
    int  runApp();
    bool init();
    void event();
    void loop();
    void render();
    void clean();
    void eatFood();
    Walls walls;
    void restartGame();
    void die();
};

#endif // _GAME_H_
