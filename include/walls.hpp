#ifndef _WALLS_H_
#define _WALLS_H_

#include <vector>

class WallCell
{
public:
    WallCell(int loc_x, int loc_y);
    int x;
    int y;
};

class Walls
{
public:
    void Init();
    bool IsWall(int sx, int sy);
    void Render();
private:
    std::vector<WallCell> cells;
};

#endif // _WALLS_H_
